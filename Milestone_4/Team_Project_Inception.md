2018-19 Toaster Code Team Project
=====================================

## Vision Statement

For people who want to get in shape or work out, the rpg-lite workout is a workout planning platform that integrates rpg elements into a workout plan. The webapp gives the user a workout plan customized and curatyed by them while providing a gamified interface and spin to all fitness activities that remsembles a role playing game. Unlike other gamified applications, our product will both focus only on a fitness expierence and provide the user with state reccommend workouts in a fun, social format.

## Summary of Our Approach to Software Development

Our approach to software development involves the Disciplined Agile Delivery method intermixed with key elements of other approaches. Namely SCRUM. We generally use the MVC 5 software architecture and outside database support. We believe in the quality of work produced by a small team of collaborating, respectful, and singly located development professionals.

## Initial Vision Discussion with Stakeholders



## Initial Requirements Elaboration and Elicitation

### Questions

1. Are the workout demos/videos stored in our database or is it a link to the video off site.

2. Is there an API that has all of the workout video details already that we can just pull from.

3. Are users allowed to use a psudeynom or are we stroing actual names?
A. psudeynom, not storing names, we are storing email address.
    
## List of Needs and Features
1. Need to be able to see what workouts are recommended for them and safe for them based upon their statistics.
2. We need to have demos/descriptions of the workouts for users
3. User accounts.
4. Display individual user workout statistics.
5. Character customization options.
6. A nice looking landing page with links/widgets to other pages.
7. The ability to invite friends to the site.
8. Adding workout goals. User selected workout goals.
9. Daily quests/workout goals. i.e. 5000 steps a day etc.
    -based on metrics, goals, possibly have tiers of quests
10. All user highscore page.
11. Set user metrics.
12. Calculate workouts appropriate for users metrics.
13. Shop to buy gear with coins earned from the dungeons.
14. Achievement page.
15. A page that lists all of the available dungeons.
16. Character view page.
17. Algorithm that proposes workouts/dungeons based on national standareds and user BMI/Metrics.
18. Integration with fitbit api.
19. The ability for users to import fitbit metrics and data.

## Initial Modeling

### Use Case Diagrams

### Other Modeling

## Identify Non-Functional Requirements

1. Sources for nationally recommended workout schedules and goals.
2. Sources for workout demos, videos, tutorials.
3. Fitbit.
4. Sources for item graphics and possibly stats.
5. Password encryption
## Identify Functional Requirements (User Stories)

E: Epic  
F: Feature  
U: User Story  
T: Task  

1. [E] Allow users to access and complete dungeons.
2. [E] Allow users to interact with their charcter.
3. [F] Allow users to log in.
4. [E] Allow users to start and complete workouts.

## Initial Architecture Envisioning

## Agile Data Modeling

## Identification of Risks
1. Fitbit API not working.
2. Absent team members.
3. Workout demo sources non-existent or unusable.
4. The Apocalypse.
5. Visual Studio's temperament

## Timeline and Release Plan